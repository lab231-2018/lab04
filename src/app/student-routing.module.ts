import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { StudentsViewComponent } from './students/view/students.view.component';
import { StudentsAddComponent } from './students/add/students.add.component';
import { StudentsComponent } from './students/list/students.component';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';


const StudentRoutes: Routes = [
  {path: 'view',component: StudentsViewComponent},
  {path: 'add',component: StudentsAddComponent},
  {path: 'list',component: StudentsComponent},
];


@NgModule({
  imports: [
    RouterModule.forRoot(StudentRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class StudentRoutingModule { }
